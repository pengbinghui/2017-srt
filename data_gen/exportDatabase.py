'''
export:
	select * from school into outfile 'D:\\AtTsinghua\\2016Spring\\SRT\\school.csv' lines terminated by '\r\n';
	select * from student into outfile 'D:\\AtTsinghua\\2016Spring\\SRT\\student.csv' lines terminated by '\r\n';

'''
import sqlite3
import csv, codecs, cStringIO

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """
    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()
    def writerow(self, row):
        self.writer.writerow([unicode(s).encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)
    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

conn = sqlite3.connect("ec_srt.db")
cur = conn.cursor()

cur.execute("SELECT * FROM school")
file = open("school.csv", "wb")
file.write("province,wenli,year,sch_name,major_name,batch,plan_no,enroll_no,min_score,max_score,avg_score\n")
writer = UnicodeWriter(file)
writer.writerows(cur)

cur.execute("SELECT * FROM student")
file = open("student.csv", "wb")
file.write("batch,province,rank,sch_name,score,score_num,major_name,wenli,year\n")
writer = UnicodeWriter(file)
writer.writerows(cur)

cur.close()
conn.commit()
conn.close()