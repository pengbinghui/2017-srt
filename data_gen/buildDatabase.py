import sqlite3

def newSplit(s):
    a = s[:-1].split(',')
    b = []
    s = ''
    for x in a:
        if s == '':
            if len(x) > 0 and x[0] == '"':
                s = x
            else:
                b.append(x)
        else:
            s += ',' + x
            if len(x) > 0 and x[-1] == '"':
                b.append(s)
                s = ''
    return b

conn = sqlite3.connect("ec_srt.db")
cur = conn.cursor()
tables = cur.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name").fetchall()
if ('school',) in tables:
    cur.execute("DROP TABLE school")
if ('student',) in tables:
    cur.execute("DROP TABLE student")
# print "delete over"
cur.execute(
    "CREATE TABLE school ( province VARCHAR(20) NOT NULL, wenli INT NOT NULL, year int NOT NULL, sch_name VARCHAR(100) NOT NULL, major_name VARCHAR(500) NOT NULL, batch INT NOT NULL, plan_no INT, enroll_no INT, min_score INT, max_score INT, avg_score INT)")
cur.execute(
    "CREATE TABLE student ( batch INT, province VARCHAR(20) NOT NULL, rank INT, sch_name VARCHAR(100), score INT, score_num INT, major_name VARCHAR(500), wenli INT NOT NULL, year INT NOT NULL)")

lines = open("sch_major_score.csv").readlines()
sch_major_score = [newSplit(l) for l in lines[1:]]
lines = open("sch_score.csv").readlines()
sch_score = [newSplit(l) for l in lines[1:]]
lines = open("sch_score_2015.csv").readlines()
sch_score_2015 = [newSplit(l) for l in lines[1:]]
lines = open("sch_major_plan_15.csv").readlines()
sch_major_plan_15 = [newSplit(l) for l in lines[1:]]
lines = open("sch_plan_15.csv").readlines()
sch_plan_15 = [newSplit(l) for l in lines[1:]]

main_batch = {
    '1': '1',
    '2': '2',
    '3': '2',
    '4': '2',
    '5': '3',
    '6': '3',
    '7': '3',
    '8': '4',
    '9': '4',
}

# print "begins"
'''i = 0
for s in sch_major_score:
    if s.__len__() != 10:
        print s
        print i
    assert(s.__len__() == 10)
    i += 1'''
for s in sch_major_score:
    '''if len(s[1]) > 100:
        print s[1]
    if len(s[8]) > 500:
        print s[8]'''
    s[3] = main_batch[s[3]]
    cur.execute(
        "INSERT INTO school (province,sch_name,wenli,batch,year,avg_score,min_score,enroll_no,major_name,max_score) VALUES ('%s', '%s', %s, %s, %s, %s, %s, %s, '%s', %s)" % tuple(
            s))
# print "school load successfully"
for s in sch_score:
    s[2] = main_batch[s[2]]
    cur.execute(
        "INSERT INTO school (wenli,year,batch,enroll_no,min_score,max_score,avg_score,sch_name,province,major_name) VALUES (%s, %s, %s, %s, %s, %s, %s, '%s', '%s', '')" % tuple(
            s))
# print "school load successfully"
for s in sch_score_2015:
    s[2] = main_batch[s[2]]
    cur.execute(
        "INSERT INTO school (province,wenli,batch,sch_name,min_score,year,major_name) VALUES ('%s', %s, %s, '%s', %s, %s, '')" % tuple(
            s))
# print "school load successfully"
for s in sch_major_plan_15:
    s[0] = main_batch[s[0]]
    cur.execute(
        "INSERT INTO school (batch,major_name,plan_no,province,sch_name,wenli,year) VALUES (%s, '%s', %s, '%s', '%s', %s, 2015)" % tuple(
            s))
# print "school load successfully"
for s in sch_plan_15:
    s[2] = main_batch[s[2]]
    cur.execute(
        "INSERT INTO school (province,wenli,batch,sch_name,plan_no,year,major_name) VALUES ('%s', %s, %s, '%s', %s, 2015, '')" % tuple(
            s))

# print "school load successfully"

lines = open("../data_gen/enroll_data.csv").readlines()
enroll_data = [newSplit(l) for l in lines[1:]]

# i = 0
for s in enroll_data:
    '''if s.__len__() != 9:
        print s
        print i
    i += 1
    assert(s.__len__() == 9)'''
    s[8] = '20' + s[8]
    if (s[0] == '0'):
        continue
    cur.execute(
        "INSERT INTO student (batch,province,rank,sch_name,score,score_num,major_name,wenli,year) VALUES (%s, '%s', %s, '%s', %s, %s, '%s', %s, %s)" % tuple(
            s))

# cur.execute("load data infile 'enroll_data.csv' into table student fields terminated by ','  optionally enclosed by '"' escaped by '"' lines terminated by '\r\n'");

conn.commit()
cur.close()
conn.close()