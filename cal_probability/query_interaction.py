# -*- coding:utf-8 -*-

import json
import sys
import random
import types
import math
import os
from scipy.stats import norm
from scipy.stats import laplace

class Query_Interaction:

    def readSchMeanAndStdev(self, dirpath):
        self.schMeanAndStdevDict = {}
        fin = open(dirpath + '/sch_mean_and_stdev.txt', 'r')
        for line in fin:
            tokens = line.strip().split('\t')
            shengyuandi = tokens[0]
            sch = tokens[1]
            wl = tokens[2]
            batch = tokens[3]
            mean = tokens[4]
            stdev = tokens[5]

            if mean != '\\N':
                mean = float(mean)
                stdev = float(stdev)
            
            if shengyuandi not in self.schMeanAndStdevDict:
                self.schMeanAndStdevDict[shengyuandi] = {}
            self.schMeanAndStdevDict[shengyuandi][sch + '_' + wl + '_' + batch] = [mean, stdev]
        fin.close()

    def __init__(self, dirpath):
        
        self.schMeanAndStdevDict = {}
        
        self.readSchMeanAndStdev(dirpath)

    def query(self, shengyuandi, sch, wl, batch, curRank):
        key = sch + '_' + wl + '_' + batch

        if shengyuandi not in self.schMeanAndStdevDict or key not in self.schMeanAndStdevDict[shengyuandi]:
            return '\\N'
        else:
            
            if self.schMeanAndStdevDict[shengyuandi][key][0] == '\\N' or self.schMeanAndStdevDict[shengyuandi][key][1] == '\\N':
				return '\\N'
            else:
				return (1.0 - laplace.cdf(curRank, self.schMeanAndStdevDict[shengyuandi][key][0], self.schMeanAndStdevDict[shengyuandi][key][1])) * 100.0

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf-8')
    dirpath = os.getcwd()
    acceptRate = Query_Interaction(dirpath)

    
    while True:
        shengyuandi = raw_input('生源地: '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
        sch = raw_input('学校: '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
        wl = raw_input('文理: '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
        batch = raw_input('批次(0:提前批, 1:第一批, 2:第二批, 3:第三批, 4:专科): '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
        rank = raw_input('学生排名: '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
    #    print acceptRate.query('广东', '中山大学', '理', '1', 4000)
        rate = acceptRate.query(shengyuandi, sch, wl, batch, int(rank))
        if rate == '\\N':
            print u'数据缺失'
        else:
            print rate
        print
    
