# 说明
这是由公司的代码和算法修改而来，目前是让其可以在我们的数据上运行
考虑到公司项目整体框架设计与我们之前不太一样，所以目前还未与我们的probability相连
query_probability.py里提供了算概率的调用接口，如果probability需要用的话可以直接调用

## 操作方法
- 将ec_srt.db文件放入本文件夹内
- 运行exportData.py，生成一个算概率所需要的文件夹，文件夹内分省csv文件，记录2012至2014年student录取信息
- 运行cal_accept_rate.py，将会生成sch_mean_and_stdev.txt文件，内有各个学校在每个省的估计录取排名和标准差；之后也可以通过命令行查概率（该文件执行完毕需二三十分钟）
- 如果运行query_interaction.py，可通过命令行来查概率。这只是为了方便做测试。
- query_probability.py里的函数用来调用得到概率