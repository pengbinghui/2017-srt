# -*- coding:utf-8 -*-

import json
import sys
import random
import types
import math
import os
from scipy.stats import norm
from scipy.stats import laplace

class AcceptRate:
    def calSafeRank(self, rankList):
        if len(rankList) >= 1000:
            fromIdx = int(len(rankList) * 0.03)
            toIdx = int(len(rankList) * 0.05)
        elif len(rankList) >= 500:
            fromIdx = int(len(rankList) * 0.04)
            toIdx = int(len(rankList) * 0.06)
        elif len(rankList) >= 300:
            fromIdx = int(len(rankList) * 0.05)
            toIdx = int(len(rankList) * 0.07)
        elif len(rankList) >= 100:
            fromIdx = int(len(rankList) * 0.06)
            toIdx = int(len(rankList) * 0.08)
        elif len(rankList) >= 50:
            fromIdx = int(len(rankList) * 0.07)
            toIdx = int(len(rankList) * 0.09)
        elif len(rankList) >= 10:
            fromIdx = int(len(rankList) * 0.08)
            toIdx = int(len(rankList) * 0.10)
        else:
            fromIdx = int(len(rankList) * 0.10)
            toIdx = int(len(rankList) * 0.20)

        totRank = 0
        for idx in range(fromIdx, toIdx + 1):
            totRank += rankList[idx]
        return totRank / (toIdx - fromIdx + 1)
                
    def readSchRankAndBatch(self, infile, errorYears):
        schScoreRankDict = {}
        schSizeDict = {}
        
        fin = open(infile, 'r')
        fin.readline()
        for line in fin:
            tokens = line.strip().split(',')
            sch = tokens[3].strip()
            if sch[0] == '"':
                sch = sch[1:-1].strip()
            if int(tokens[7]) == 1:
                wl = '文'
            else:
                wl = '理'
            batch = int(tokens[0])
            scoreNum = int(tokens[5])
            score = int(tokens[4])
            year = int(tokens[8])
            rank = int(tokens[2])
            major = tokens[6]

            if scoreNum == 0: #浙江的数据有问题, score_number都是0
                continue

            if year in errorYears: #部分省份最近才开始平行志愿,某些年份不可用
                continue

            key = sch + '_' + wl + '_' + str(batch)
            if key not in schScoreRankDict:
                schScoreRankDict[key] = [[], [], []]
                schSizeDict[key] = [0, 0, 0]
            for i in range(max(1, scoreNum)):
                schScoreRankDict[key][year - 2012].append([score, rank])
            schSizeDict[key][year - 2012] += scoreNum

        fin.close()

        schTmpRankDict = {}
        schMediumRankDict = {}
        schSafeRankDict = {}
        schLastRankDict = {}
        for (k, v) in schScoreRankDict.items():
            tmpRankList = []
            mediumRankList = []
            safeRankList = []
            lastRankList = []
            for i in range(len(v)):
                if len(v[i]) > 0:
                    lst = sorted(v[i], lambda x, y: cmp(x[1], y[1]))
                    tmpRank = lst[int(len(lst) * 0.625)][1]
                    mediumRank = lst[int(len(lst) * 0.5)][1]
                    lastRank = lst[len(lst) - 1][1]
                    
                    scoreList = []
                    rankList = []
                    for (score, rank) in v[i]:
                        scoreList.append(score)
                        rankList.append(rank)
                    scoreList = sorted(scoreList, lambda x, y: cmp(x, y), reverse = True)

                    if len(scoreList) <= 2:
                        safeRank = lastRank
                    else:
                        ######计算安全线######
                        while True:
                            mean, stdev = norm.fit(scoreList)
                            isEnd = True
                            idx = len(scoreList) - 1
                            while idx > 0 and norm.cdf(scoreList[idx], mean, stdev) < 0.03:
                                scoreList.pop()
                                idx -= 1
                                if len(v[i]) - len(scoreList) > 50 or 1.0 * len(scoreList) / len(v[i]) < 0.7:
                                    isEnd = True
                                    break
                                else:
                                    isEnd = False
                            if isEnd:
                                break
                        
                        for (score, rank) in v[i]:
                            if score == scoreList[idx]:
                                safeRank = rank
                                break

                    rankList = sorted(rankList, lambda x, y: cmp(x, y), reverse = True)
                    safeRank = min(safeRank, self.calSafeRank(rankList)) #左老师的经验和高斯相结合
                    
                else:
                    tmpRank = '\\N'
                    mediumRank = '\\N'
                    safeRank = '\\N'
                    lastRank = '\\N'

                tmpRankList.append(tmpRank)
                mediumRankList.append(mediumRank)
                safeRankList.append(safeRank)
                lastRankList.append(lastRank)

            schTmpRankDict[k] = tmpRankList
            schMediumRankDict[k] = mediumRankList
            schSafeRankDict[k] = safeRankList
            schLastRankDict[k] = lastRankList

        return schTmpRankDict, schMediumRankDict, schSafeRankDict, schLastRankDict, schSizeDict
    
    def calWeightedValue(self, lst):
        totalValue = 0.0
        totalValueWeight = 0.0
        for i in range(len(lst)):
            if lst[i] != '\\N':
                totalValue += lst[i] * (i + 1)
                totalValueWeight += (i + 1)
        if totalValueWeight == 0.0:
            return '\\N'
        else:
            return totalValue / totalValueWeight
    
    def calSchMeanAndStdev(self, provChn, schMediumRankDict, schSafeRankDict, schLastRankDict, schSizeDict, alpha0, alpha1, alpha2):
        schMeanAndStdevDict = {}
        delta = 0.0
        total = 0
        for (key,  mediumRankList) in schMediumRankDict.items():
            sch, wl, batch = key.split('_')

            weightedMediumRank = self.calWeightedValue(mediumRankList)
            if weightedMediumRank == '\\N':
                schMeanAndStdevDict[key] = ['\\N', '\\N']
                continue
            weightedSafeRank = self.calWeightedValue(schSafeRankDict[key])
            size = self.calWeightedValue(schSizeDict[key])
            
            weightedRankList = []
            for (otherKey, otherMediumRankList) in schMediumRankDict.items():
                otherSch, otherWl, otherBatch = otherKey.split('_')
                if wl != otherWl or batch != otherBatch:
                    continue
                
                otherWeightedMediumRank = self.calWeightedValue(otherMediumRankList)
                if otherWeightedMediumRank == '\\N':
                    continue
                otherSafeRankList = schSafeRankDict[otherKey]
                otherWeightedSafeRank = self.calWeightedValue(otherSafeRankList)
                otherSize = self.calWeightedValue(schSizeDict[otherKey])
                
                sim = norm.pdf(alpha0 * (size - otherSize) / min(size, otherSize)) * (norm.pdf(alpha1 * (weightedMediumRank - otherWeightedMediumRank) / min(weightedMediumRank, otherWeightedMediumRank)) + norm.pdf(alpha2 * (weightedSafeRank - otherWeightedSafeRank) / min(weightedSafeRank, otherWeightedSafeRank))) # 招生规模相似度 * (中位排名相似度 + 安全线排名相似度)

                for i in range(len(otherSafeRankList) - 1):
                    if otherSafeRankList[i] == '\\N' or otherSafeRankList[i + 1] == '\\N':
                        continue
                    weightedRankList.append([sim, otherSafeRankList[i + 1] - otherSafeRankList[i], otherKey]) #相似学校权重， 2014 - 2013 安全线排名 or 2013 - 2012

            if len(weightedRankList) <= 1:
                schMeanAndStdevDict[key] = ['\\N', '\\N']
                continue
            
            totalSim = 0.0
            weightedRankList = sorted(weightedRankList, lambda x, y: cmp(x[0], y[0]), reverse = True)
            for i in range(min(50, len(weightedRankList))):
                if weightedRankList[i][0] < 1e-8:
                    break
                totalSim += weightedRankList[i][0]

            if totalSim < 1e-8:
                schMeanAndStdevDict[key] = ['\\N', '\\N']
                continue
            # Laplace
            sumAbs = 0.0
            for i in range(min(50, len(weightedRankList))):
                sumAbs += weightedRankList[i][0] * abs(weightedRankList[i][1])
            stdev = sumAbs / totalSim

            schMeanAndStdevDict[key] = [weightedSafeRank, stdev] #安全线排名作为中位数

        return schMeanAndStdevDict

    def printSchMeanAndStdev(self, dirpath):
        fout = open(dirpath + '/sch_mean_and_stdev.txt', 'w')
        for (shengyuandi, schMeanAndStdevDict) in self.schMeanAndStdevDict.items():
            for (k, v) in schMeanAndStdevDict.items():
                sch, wl, batch = k.split('_')
                fout.write('%s\t%s\t%s\t%s\t' %(shengyuandi, sch, wl, batch))
                '''
                mediumRankList = self.schMediumRankDict[shengyuandi][k]
                for rank in mediumRankList:
                    fout.write('%s\t' %str(rank))
                safeRankList = self.schSafeRankDict[shengyuandi][k]
                for rank in safeRankList:
                    fout.write('%s\t' %str(rank))
                lastRankList = self.schLastRankDict[shengyuandi][k]
                for rank in lastRankList:
                    fout.write('%s\t' %str(rank))
                '''
                if v[0] == '\\N':
                    fout.write('\\N\t\\N\n')
                else:
                    fout.write('%.3f\t%.3f\n' %(v[0], v[1]))
        fout.close()

    def readSchMeanAndStdev(self, dirpath):
        self.schMeanAndStdevDict = {}
        fin = open(dirpath + '/sch_mean_and_stdev.txt', 'r')
        for line in fin:
            tokens = line.strip().split('\t')
            shengyuandi = tokens[0]
            sch = tokens[1]
            wl = tokens[2]
            batch = tokens[3]
            mean = tokens[4]
            stdev = tokens[5]

            if mean != '\\N':
                mean = float(mean)
                stdev = float(stdev)
            
            if shengyuandi not in self.schMeanAndStdevDict:
                self.schMeanAndStdevDict[shengyuandi] = {}
            self.schMeanAndStdevDict[shengyuandi][sch + '_' + wl + '_' + batch] = [mean, stdev]
        fin.close()

    def __init__(self, dirpath):
        
        self.provList = ['an_hui', 'bei_jing', 'chong_qing', 'fu_jian', 'gan_su', 'guang_dong', 'guang_xi', 'gui_zhou', 'hai_nan', 'he_bei', 'hei_long_jiang', 'he_nan', 'hu_bei', 'hu_nan', 'jiang_su', 'jiang_xi', 'ji_lin', 'liao_ning', 'nei_meng_gu', 'ning_xia', 'shan_dong', 'shan_xi', 'si_chuan', 'tian_jin', 'xin_jiang', 'xi_shan', 'yun_nan', 'zhe_jiang']
        self.provChnList = ['安徽', '北京', '重庆', '福建', '甘肃', '广东', '广西', '贵州', '海南', '河北', '黑龙江', '河南', '湖北', '湖南', '江苏', '江西', '吉林', '辽宁', '内蒙古', '宁夏', '山东', '陕西', '四川', '天津', '新疆', '山西', '云南', '浙江']
        
        self.schTmpRankDict = {}
        self.schMediumRankDict = {}
        self.schSafeRankDict = {}
        self.schLastRankDict = {}
        self.schSizeDict = {}
        self.schMeanAndStdevDict = {}
        
        for i in range(len(self.provList)):
            if self.provList[i] == 'bei_jing' or self.provList[i] == 'hu_bei':
                errorYears = [2012, 2013]
            elif self.provList[i] == 'shan_dong' or self.provList[i] == 'hei_long_jiang':
                errorYears = [2012]
            else:
                errorYears = []
            
            schTmpRankDict, schMediumRankDict, schSafeRankDict, schLastRankDict, schSizeDict = self.readSchRankAndBatch(dirpath + '/prov_student/' + self.provList[i] + '.csv', errorYears)

            self.schTmpRankDict[self.provChnList[i]] = schTmpRankDict
            self.schMediumRankDict[self.provChnList[i]] = schMediumRankDict
            self.schSafeRankDict[self.provChnList[i]] = schSafeRankDict
            self.schLastRankDict[self.provChnList[i]] = schLastRankDict
            self.schSizeDict[self.provChnList[i]] = schSizeDict
            
            self.schMeanAndStdevDict[self.provChnList[i]] = self.calSchMeanAndStdev(self.provChnList[i], schMediumRankDict, schSafeRankDict, schLastRankDict, schSizeDict, 0.4, 0.5, 0.4)

        self.printSchMeanAndStdev(dirpath)
        
        self.readSchMeanAndStdev(dirpath)

    def query(self, shengyuandi, sch, wl, batch, curRank):
        key = sch + '_' + wl + '_' + batch

        if shengyuandi not in self.schMeanAndStdevDict or key not in self.schMeanAndStdevDict[shengyuandi]:
            return '\\N'
        else:
            '''
            size = self.schSizeDict[shengyuandi][key]
            tmpRankList = self.schTmpRankDict[shengyuandi][key]
            mediumRankList = self.schMediumRankDict[shengyuandi][key]
            safeRankList = self.schSafeRankDict[shengyuandi][key]
            lastRankList = self.schLastRankDict[shengyuandi][key]
            
            print 
            print u'======分析======'
            
            print u'往年招生人数:'
            for i in range(len(size)):
                print (2012 + i), size[i], '\t'
            print

            print u'往年中位学生排名:'
            for i in range(len(mediumRankList)):
                print (2012 + i), mediumRankList[i], '\t'
            print

            print u'往年最后37.5%学生排名:'
            for i in range(len(tmpRankList)):
                print (2012 + i), tmpRankList[i], '\t'
            print
            
            print u'往年安全线学生排名:'
            for i in range(len(safeRankList)):
                print (2012 + i), safeRankList[i], '\t'
            print u'平均', self.schMeanAndStdevDict[shengyuandi][key][0]
            print
            
            print u'往年最低分学生排名:'
            for i in range(len(lastRankList)):
                print (2012 + i), lastRankList[i], '\t'
            print
			'''
            if self.schMeanAndStdevDict[shengyuandi][key][0] == '\\N' or self.schMeanAndStdevDict[shengyuandi][key][1] == '\\N':
				return '\\N'
            else:
				'''
				print u'方案:'
				print u'聚类标准差:', self.schMeanAndStdevDict[shengyuandi][key][1]
				print u'波动:', curRank - self.schMeanAndStdevDict[shengyuandi][key][0]
				print '%.3f%%' %((1.0 - laplace.cdf(curRank, self.schMeanAndStdevDict[shengyuandi][key][0], self.schMeanAndStdevDict[shengyuandi][key][1])) * 100.0)
				print
				'''
				return (1.0 - laplace.cdf(curRank, self.schMeanAndStdevDict[shengyuandi][key][0], self.schMeanAndStdevDict[shengyuandi][key][1])) * 100.0

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf-8')
    dirpath = os.getcwd()
    acceptRate = AcceptRate(dirpath)

    
    while True:
        shengyuandi = raw_input('生源地: '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
        sch = raw_input('学校: '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
        wl = raw_input('文理: '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
        batch = raw_input('批次(0:提前批, 1:第一批, 2:第二批, 3:第三批, 4:专科): '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
        rank = raw_input('学生排名: '.decode('utf-8').encode('gbk')).decode(sys.stdin.encoding).encode('utf-8')
    #    print acceptRate.query('广东', '中山大学', '理', '1', 4000)
        rate = acceptRate.query(shengyuandi, sch, wl, batch, int(rank))
        if rate == '\\N':
            print u'数据缺失'
        else:
            print rate
        print
    
